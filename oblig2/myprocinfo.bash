#!/bin/bash

clear		#Clear skjerm

meny()		#Funksjon som printer meny
{
echo '1 - Hvem er jeg og hva er navnet på dette skriptet?'
echo '2 - Hvor lenge er det siden siste reboot?'
echo '3 - Hvor mange prosesser og tråder finnes?'
echo '4 - Hvor mange context switcher fant sted siste sekund?'
echo '5 - Hvor stor andel av CPU-tiden ble benyttet i kernel mode og i user mode siste sekund?'
echo '6 - Hvor mange interrupts fant sted siste sekund?'

echo '9 - Avslutt dette scriptet!'
}

meny		#Print meny
echo -e '\nVelg en funksjon: '

read "valg"	#Les inn valg

while [ "$valg" != "9" ];		#Loop mens valg ikke er 9
do

case "$valg" in			#Gjør case valg tilsvarer


'1')
#Hvem jeg er og hva scriptet heter
	echo -n -e '	Jeg er: 	'; whoami;
	echo -n '	Skriptet heter: ' "$(basename -- "$0")";
	echo -e '\n'
	;;

'2')
#Tid til forige reboot
	echo -n '	'; uptime -p;
	echo -e -n '\n'
	;;

'3')
#Hvor mange prosesser og tråder det finnes - må header taes vekk? hæææææææ?
	echo -n '	Antall prosesser: '; ps -aux --no-heading | wc -l;
	echo -n '	Antall tråder:    '; ps -eLF --no-heading | wc -l;
	echo -n -e '\n'
	;;

'4')
#echo hvor mange context switcher siste sekund
	en=$(grep ctxt /proc/stat | awk '{print $2}');
	sleep 1
	to=$(grep ctxt /proc/stat | awk '{print $2}');
	let tre="$to"-"$en";
	echo -n '	Antall context switcher siste sekund:' "$tre";
	echo -n -e '\n';
	;;

'5')
#CPU tid siste sekund
	usera=$(grep -w  cpu /proc/stat | awk '{print $2}');
	kernela=$(grep -w cpu /proc/stat | awk '{print $4}');

	sleep 1;

	userb=$(grep -w cpu /proc/stat | awk '{print $2}');
	kernelb=$(grep -w cpu /proc/stat | awk '{print $4}');

	let userc="$userb"-"$usera";
	let kernelc="kernelb"-"kernela";

	echo     '	CPU tid benyttet i user mode:  ' "$userc";
	echo -n	 '	CPU tid benyttet i kernel mode:' "$kernelc";
	echo -e '\n';

	;;

'6')
#Hvor mange interrupts siste sekund
	en=$(grep intr /proc/stat | awk '{print $2}');
	sleep 1;
	to=$(grep intr /proc/stat | awk '{print $2}');
	let tre="$to"-"$en"
	echo -n '	Antall interrupts siste sekund:' "$tre" ;
	echo -n -e '\n';
	;;


*)
#Om valg ikke tilsvarer en case eller avslutter
	echo -e '\n	Se meny for valg!'
	;;

esac		#Slutten på case

		#Ny case?
echo -e '\nVelg en funksjon: '
read "valg"


done		#Slutten på while
