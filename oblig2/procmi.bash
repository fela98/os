#!/bin/bash


for i in "$@" #
do


#Lag .meminfo fil
   fil="$i-$(date +"%d.%m.%Y").meminfo"
   touch "$fil"

   echo "********** Minne info om prosess med pid  $i **********" > "$fil"
   echo -e -n "\n" >> "$fil"

#Totalt virtuelt minne brukt:
   totvirmem=$(cat /proc/"$i"/status | grep VmSize | awk '{print $2 " " $3}')
   echo "Totalt bruk av virtuelt minne:	 $totvirmem" >> "$fil"


#Mengde privat virtuelt minne
   vmdata=$(cat /proc/"$i"/status | grep VmData | awk '{print $2}')
   vmstk=$(cat /proc/"$i"/status | grep VmStk | awk '{print $2}')
   vmexe=$(cat /proc/"$i"/status | grep VmExe | awk '{print $2}')

   privmem=$((vmdata+vmstk+vmexe))
   privmem="$privmem kB"
   echo "Mengde virtuelt minne som er privat:  $privmem" >> "$fil"


#Mengde delt virtuelt minne
   deltmem=$(cat /proc/"$i"/status | grep VmLib | awk '{print $2 " " $3}')
   echo "Mengde virtuelt minne som er delt:   $deltmem" >> "$fil"


#Totalt fysisk minne brukt
   phymem=$(cat /proc/"$i"/status | grep VmRSS | awk '{print $2 " " $3}')
   echo "Totalt bruk av fysisk minne:	$phymem" >> "$fil"

#Page table fysisk minne
   pagetable=$(cat /proc/"$i"/status | grep VmPTE | awk '{print $2 " " $3}')
   echo "Fysisk minne som benyttes til page table:  $pagetable" >> "$fil"


done

