	Obligatoriske oppgaver i operativsystemer, IMT2282.
 
Oppgaver lagres på bitbucket i hver sin mappe: oblig1, oblig2, oblig3.
Oppgavene navngis etter oppgavenavn i kompendiet, med mindre oppgaven sier annet.
Hver oblig inneholder et dokument med hvordan vi sikret kodekvalitet.

Gruppemedlemmer:
	- Kristian Alsgaard Skaue
	- Olav Henrik Hoggen
	- Felix Schoeler
	-
