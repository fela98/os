﻿#myprocinfo.ps1


function menu {
    Write-Output '1 - Hvem er jeg og hva er navnet p°a dette scriptet?'
    Write-Output '2 - Hvor lenge er det siden siste boot?'
    Write-Output '3 - Hvor mange prosesser og tr°ader finnes?'
    Write-Output '4 - Hvor mange context switcher fant sted siste sekund?'
    Write-Output '5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?'
    Write-Output '6 - Hvor mange interrupts fant sted siste sekund?'
    Write-Output '9 - Avslutt dette scriptet'

}


Clear-Host

menu

$a = Read-Host 'Valg: '
Write-Output ' '
Write-Output ' '
switch ($a)
{


#1 - Hvem er jeg og hva er navnet på dette scriptet?
"1"{
    $scriptName = $MyInvocation.MyCommand.Name
    Write-Output "Jeg er $env:Username og dette scriptet er $scriptName"
}


#2 - Hvor lenge er det siden siste boot?
"2"{
    Write-Output "Hvor lenge siden forige boot: " 
    (get-date) - (Get-CimInstance Win32_OperatingSystem).LastBootUpTime
}


#3 - Hvor mange prosesser og tråder finnes?
"3"{
    $totProcesses = $(Get-Process).count
    $totThreads = $(Get-CimInstance win32_thread).count

    Write-Output "Det er $totProcesses prosesser og $totThreads tråder"
}


#4 - Hvor mange context switch'er fant sted siste sekund?
"4"{
    $contextSwitches = ((get-counter -Counter "\System\Context Switches/sec").CounterSamples).CookedValue
    Write-Output "Det var $contextSwitches context switcher i siste sekund"
}


#5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
"5"{
    $kernel = ((Get-Counter "\Processor(_total)\% Privileged Time").CounterSamples).CookedValue
    $user =  ((Get-Counter "\Processor(_total)\% User Time").CounterSamples).CookedValue
    Write-Output "$kernel ble brukt i kernelmode og $user ble brukt i user mode"
}


#6 - Hvor mange interrupts fant sted siste sekund?
"6"{
    $interrupts = ((Get-Counter "\Processor(_total)\Interrupts/sec").CounterSamples).CookedValue
    Write-Output "Det var $interrupts interrupts i siste sekund"
}

}
