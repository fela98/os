﻿foreach ($PrID in $args) {
    $process = Get-Process -Id $PrID -ea SilentlyContinue
    if ($null -ne $process) {
@"
******** Minne info om prosess med PID $PrID ********
Total bruk av virtuelt minne: $(($process.VirtualMemorySize/1MB).Tostring(".00")) MB
Størrelse pa Working Set:" $(($process.WorkingSet/1MB).Tostring(".00")) MB
"@ | Out-File -filePath "./$PrID$(Get-Date -format "-yyyyMMddHHmmss").meminfo"
    } else {
        Write-Output "Det finnes ingen kjørende prosess med id $PrID"
    }
}