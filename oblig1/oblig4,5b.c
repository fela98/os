
#include <stdio.h>		/* Printf */
#include <stdlib.h>		/* Exit */
#include <unistd.h>		/* Fork */
#include <sys/wait.h>		/* Waitpid */
#include <sys/types.h>		/* Pid_t */

void process(int number, int time) {
  printf("Process %d running\n", number);
  sleep(time);
  printf("Process %d ran for %d seconds\n", number, time);
}

int main(void) {
  pid_t p[6];			// 6 variables to store pid

  p[0]=fork();
  if(p[0]==0) {
    process(0,1);		// Run process 0
    exit(0);
  }

  p[2]=fork();
  if(p[2]==0) {
    process(2,3);		// Run process 2
    exit(0);
  }

  waitpid(p[0],NULL,0);		// Wait for process 0

  p[1]=fork();
  if(p[1]==0){
    process(1,2);		// Run process 1
    exit(0);
  }

  p[4]=fork();
  if(p[4]==0){
  process(4,3);			// Run process 4
  exit(0);
  }

  waitpid(p[2],NULL,0);		// Wait for process 2

  p[3]=fork();
  if(p[3]==0){
    process(3,2);		// Run process 3
    exit(0);
  }

  waitpid(p[4],NULL,0);		// Wait for process 4

  p[5]=fork();
  if(p[5]==0) {
    process(5,3);		// Run process 5
    exit(0);
  }

  waitpid(p[5],NULL,0);		// Wait for p[5] before terminating

return 0;

}


